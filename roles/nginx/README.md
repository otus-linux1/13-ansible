nginx
=========

устанавливает nginx из официального репозитория и запускает его на заданном порту

Role Variables
--------------

- nginx_port: порт на котором будет слушать nginx


Example Playbook
----------------

Including an example of how to use your role (for instance, with variables passed in as parameters) is always nice for users too:

- hosts: servers
  roles:
    - nginx
  become: true

License
-------

MIT

Author Information
------------------

Roman Bolkhovitin
